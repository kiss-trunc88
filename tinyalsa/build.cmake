#!/bin/sh -e

export DESTDIR="$1"

cmake -S . -B build \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_BUILD_TYPE=None \
    -DTINYALSA_BUILD_EXAMPLES=OFF \
    -DTINYALSA_BUILD_UTILS=ON \
    -DTINYALSA_USES_PLUGINS=ON

cmake --build build
cmake --install build
